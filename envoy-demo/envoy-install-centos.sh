#/bin/bash

#installation

yum install yum-utils -y;
rpm --import 'https://rpm.dl.getenvoy.io/public/gpg.CF716AF503183491.key';
curl -sL 'https://rpm.dl.getenvoy.io/public/config.rpm.txt?distro=el&codename=7' > /tmp/tetrate-getenvoy-rpm-stable.repo;
yum-config-manager --add-repo '/tmp/tetrate-getenvoy-rpm-stable.repo';
yum makecache --disablerepo='*' --enablerepo='tetrate-getenvoy-rpm-stable';
yum install getenvoy-envoy -y


# 설치후 default config의 dns_lookup_family 
# 아래 connect_timeout 추가할 것 
# (with versions < 1.19 the connect_timeout setting is required, for later versions it has a default)

# connect_timeout: 5s

# run
# envoy -c ./envoy-demo.yaml